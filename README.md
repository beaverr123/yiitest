Installation steps:

1. $ php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations
2. $ php yii migrate/up

3. $ bower install eonasdan-bootstrap-datetimepicker#latest --save
4. $ bower install moment --save
