$(function () {
    $('form').on('submit', function(e){
        e.preventDefault();

        var form = $(this);
        var formData = form.serialize();

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                console.log(data);
                var res = JSON.parse(data);
                $("#urlHolder span").html(res.short_url);
                $("#urlHolder").show();

                $('input[name="url"]').val('');
                $('input[name="ttl"]').val('');
            },
            error: function () {
                alert("Something went wrong");
            }
        });
    });

    $('input[name="setttl"]').click(function() {
        $('#datetimepicker').toggle(this.checked);
    });

    $('input[name="ttl"]').datetimepicker({
        format:'YYYY-MM-DD HH:mm:00'
    });
});