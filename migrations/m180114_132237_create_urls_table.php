<?php

use yii\db\Migration;

/**
 * Handles the creation of table `urls`.
 */
class m180114_132237_create_urls_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('urls', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'fullUrl' => $this->string()->notNull(),
            'urlHash' => $this->string()->notNull()->unique(),
            'ttl' => $this->dateTime(),
            'viewsCount' => $this->integer()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-url-urlHash',
            'urls',
            'urlHash'
        );

        $this->createIndex(
            'idx-url-user_id',
            'urls',
            'user_id'
        );

        $this->addForeignKey(
            'fk-url-user_id',
            'urls',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('urls');
    }
}
