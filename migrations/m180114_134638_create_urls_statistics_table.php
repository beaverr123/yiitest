<?php

use yii\db\Migration;

/**
 * Handles the creation of table `urls_statistics`.
 */
class m180114_134638_create_urls_statistics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('urls_statistics', [
            'id' => $this->primaryKey(),
            'url_id' => $this->integer()->notNull(),
            'datetime' => $this->dateTime(),
            'userAgentInfo' => $this->string(),
            'geoInfo' => $this->string()
        ]);

        $this->createIndex(
            'idx-url-url_id',
            'urls_statistics',
            'url_id'
        );

        $this->addForeignKey(
            'fk-url-url_id',
            'urls_statistics',
            'url_id',
            'urls',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('urls_statistics');
    }
}
