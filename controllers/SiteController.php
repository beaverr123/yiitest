<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use yii\helpers\Url;
use yii\web\HttpException;
use app\models\Urls;
use app\models\UrlsStatistics;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','urlsstatistics'],
                'rules' => [
                    [
                        'actions' => ['logout','shorten','urlsstatistics'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionShorten()
    {
        $request = Yii::$app->request;
        $url = $request->post('url');
        $setttl = $request->post('setttl');

        if($request->isAjax && $url){
            $hashids = Yii::createObject([
                'class' => 'light\hashids\Hashids'
            ]);

            $urlObj = new Urls();
            $urlObj->save();
            $urlObj->refresh();

            $urlObj->fullUrl = $url;
            $urlObj->urlHash = $hashids->encode($urlObj->id);

            if($setttl){
                $urlObj->ttl = $request->post('ttl');
            }

            if(!yii::$app->user->isGuest){
                $urlObj->user_id = yii::$app->user->id;
            }

            $urlObj->save();

            return json_encode(['short_url'=>Url::to(['/su/'.$urlObj->urlHash], true)]);
        }else{
            throw new HttpException(400,'Invalid request. Please do not repeat this request again');
        }
    }

    public function actionUrlviewer($urlHash)
    {
        if($urlObj = Urls::findOne(['urlHash'=>$urlHash])){
            if($urlObj->ttl && $urlObj->ttl < date('Y-m-d H:i:s')){
                throw new HttpException(404,'Invalid URL');
            }

            $urlObj->viewsCount++;
            $urlObj->save();

            if($urlObj->user_id){
                $urlsStatisticsObj = new UrlsStatistics();
                $urlsStatisticsObj->url_id = $urlObj->id;
                $urlsStatisticsObj->datetime = date('Y/m/d H:i:s');

                $request = new yii\web\Request();
                $urlsStatisticsObj->userAgentInfo = $request->getUserAgent();
                $urlsStatisticsObj->geoInfo = yii::$app->geolocation->getInfo();
                $urlsStatisticsObj->save();
            }

            $this->redirect($urlObj->fullUrl);
        }else{
            throw new HttpException(404,'Invalid URL');
        }
    }

    public function actionUrlsstatistics()
    {
        $urls = Urls::find()->where(['user_id'=>yii::$app->user->id])->with('urlsStatistics')->all();

        return $this->render('urlsStatistics',[
            'urls' =>$urls
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
