<?php

namespace app\models;

use yii\db\ActiveRecord;

class Urls extends ActiveRecord
{

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{urls}}';
    }

    public function getUrlsStatistics()
    {
        return $this->hasMany(UrlsStatistics::className(), ['url_id' => 'id']);
    }
}