<?php

namespace app\models;

class MyUser extends User
{
    public function getUrls() {

        return $this->hasMany(Urls::className(), ['user_id' => 'id']);
    }
}
