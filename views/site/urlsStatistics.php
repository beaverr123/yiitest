<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Url statistics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Statistics of shorted URLs
    </p>

    <table class="table">
        <thead>
        <tr>
            <th></th>
            <th scope="col">Full URL</th>
            <th scope="col">Short URL</th>
            <th scope="col">views Count</th>
        </tr>
        </thead>
        <tbody>
        <?php if(count($urls)>0):?>
            <?php foreach($urls as $url):?>
        <tr>
            <td data-toggle="collapse" data-target=".url<?php echo $url->id ?>" ><?php echo (count($url->urlsStatistics) > 0) ? '<a href="#">expand/collapse</a>':'';?></td>
            <td><a target="_blank" href="<?php echo $url->fullUrl ?>"><?php echo $url->fullUrl ?></a></td>
            <td><a target="_blank" href="<?php echo Url::to(['/su/'.$url->urlHash], true); ?>"><?php echo Url::to(['/su/'.$url->urlHash], true); ?></a></td>
            <td><?php echo $url->viewsCount ?></td>
        </tr>

        <?php if(count($url->urlsStatistics) > 0): ?>
        <tr>
            <td colspan="4">
                <table class="table collapse url<?php echo $url->id ?>">
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Useragent info</th>
                    <th scope="col">geo info</th>
                </tr>

                    <?php foreach($url->urlsStatistics as $statistics):?>
                        <tr>
                            <td><?php echo $statistics->datetime ?></td>
                            <td><?php echo $statistics->userAgentInfo ?></td>
                            <td><?php print_r( json_decode($statistics->geoInfo)) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </td>
        </tr>
        <?php endif; ?>

            <?php endforeach; ?>
        <?php else:?>
            <tr>
                <td colspan="3">You have no short URLs</td>
            </tr>
        <?php endif?>
        </tbody>
    </table>
</div>


<style>
    tr.collapse.in {
        display:table-row;
    }
</style>