<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\DatetimepickerAsset;
use app\assets\MomentjsAsset;

DatetimepickerAsset::register($this);
MomentjsAsset::register($this);

$this->title = 'My Yii Application';
?>
<div class="container">

    <div class="jumbotron">
        <h2>Create short URL and watch statistics!</h2>

        <form action="<?php echo Url::to(['/site/shorten'], true); ?>" method="post" class="form-horizontal">
            <div class="form-group">
                <input required type="text" name="url" placeholder="http://">
            </div>
            <div class="form-group">
                <label for="setttl">Set time to life</label>
                <input id="setttl" type="checkbox" name="setttl" />
            </div>
            <div class="form-group " id="datetimepicker" style="display: none">
                <div class="input-group date col-sm-3 center-block">
                    <input type='text' name="ttl" class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                </div>
            </div>
            <div class="row">
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                <button type="submit">Create short URL</button>
            </div>
        </form>

        <div id="urlHolder"  style="display: none">
            <h2>Your short URL <span></span></h2>
            <?php if(Yii::$app->user->isGuest):?>
                <p>If you want to collect statistics <a href="<?php echo Url::to(['/user/registration/register'], true); ?>">register</a> and make short URLs</p>
            <?php else: ?>
                <p><a href="<?php echo Url::to(['/site/urlsstatistics'], true); ?>">Watch statistics</a> of your URL</p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/js/index.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>